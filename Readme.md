# Box - Event Long Polling Client

Listen to your Box events i.e actions that affect files, folders, and other Box content.

##Prerequisites 
* [Generate a Developer Token](https://app.box.com/developers/console/app/213558/configuration) - A developer token allows you to use the Box API to access your personal Box account only. This token is valid for 60 minutes.

### To Run

To Run, simply use the bundled gradle wrapper to build

```
gradle run  -Pbox_token="['Box_Developer_Token']"
```


Example:

```
gradle run  -Pbox_token="['msyVv3ogBjEVDOJ1gZWlnQXy1cyUR0Sy']"
```

End with an example of getting some data out of the system or using it for a little demo

## Design

This client has been written in Groovy. Groovy is 
The com.box.longpoll.LongPoll.groovy 

## Workflow

      
        1. Make a note of the current stream position
        2. Retrieve the URL to make a Long Poll Request and wait for a response
        3. Any changes made to your files will trigger an event. The Long Poll request #2 will yeild a response
        4. Query for the Event data by making a request from the previously noted position (from Step #1)
        5. Repeat these steps until the program is terminated explicitly
        

## Notes

 - Unit tests are a work in progress. 
 - Please note that this has not been written as a Production grade application. 
 - Considerations like Logging, Configuration management (Server URLs, Context Paths etc. are yet to be moved out of the Sources)
 - User Data validation, Handling of API errors, API response etc. could be improved with additional checks. 


## Built With

* [Groovy](http://groovy-lang.org/) - Groovy is a powerful, optionally typed and dynamic language, with static-typing and static compilation capabilities for the JVM.
* [Gradle](https://gradle.org/) - Gradle is an open-source build automation tool focused on flexibility and performance. Gradle build scripts are written using a Groovy or Kotlin DSL.
* [Jodd](https://jodd.org/http/) - A tiny yet simple and elegant HTTP Client
* [Gson](https://github.com/google/gson/) - Gson is a library that can be used to convert Objects into their JSON representation and vice versa
* [Box APIs](https://developer.box.com/docs/using-long-polling-to-monitor-events)


## References

* [Box - Event Long Polling](https://developer.box.com/docs/using-long-polling-to-monitor-events)


## Note to the Reviewers

- Special thanks to the Interviewers for giving me the opportunity to write this Client
- This helped me understand your APIs well and also gave me a reason to learn Groovy