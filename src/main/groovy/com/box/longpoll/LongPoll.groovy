package com.box.longpoll

import com.google.gson.Gson
import jodd.http.HttpRequest
import jodd.http.HttpResponse

/*
This class continuously listens to the Box's API for events and retrieves them using the Long Polling HTTP technique.

Box's API Details: https://developer.box.com/docs/using-long-polling-to-monitor-events
 */

class LongPoll {
    /* Entry point of the application */

    static void main(String[] args) {

        final def TOKEN = args[0]
        final def HOST = "api.box.com"

        while (true) {

            //Step 1: Get the current stream position

            //Make a call to the Box's API to retrieve the current steam position metadata
            def final streamPositionResponse = httpRequest(TOKEN, HOST, "/2.0/events?stream_position=now", "GET")
            println("Obtained event position JSON string ${streamPositionResponse}")

            //Convert the response String to an instance of the Position.class
            def final streamPositionObj = convertStrToJson(streamPositionResponse, Position.class)

            //Extract the position from the above Position instance
            def final position = streamPositionObj.next_stream_position.toString()
            println("Current Stream position is ${position}")

            //Step 2: Retrieve the URL to start a Long Poll Request
            def final longPollReqResponse = httpRequest(TOKEN, HOST, "/2.0/events", "OPTIONS")

            /* Convert the above response to a Map (Dictionary) and navigate it properly to get the URL. The "entries" key has a value of "List" Type. As per Box's API, the first item in this collection is the URL we are looking for.*/
            def longPollMap = convertStrToJson(longPollReqResponse, Map.class)
            def final longPollUrl = longPollMap.entries.get(0).url
            println("URL to make a Long Poll request is ${longPollUrl}")

            /* The httpRequest method that we defined requires us to pass in the HOST, PATH and the METHOD etc. separately.
             * So we will need to extract these parts from the full URL */
            def final longPollURI = new URI(longPollUrl)
            // Begin a Long Poll request
            //Step 3: Any changes made to your Box content will trigger an event and return a response for the following request
            println("Listening for an event through a Long Poll Request.Any changes made to your Box's content will trigger an event.\nListening......")
            def
            final longPollResp = httpRequest(TOKEN, longPollURI.getHost(), longPollURI.getPath() + "?" + longPollURI.getQuery(), "GET")
            println("A new event has been received: ${longPollResp}")

            //Step 4: Retrieve the event from the previously documented Position
            def final eventDetails = httpRequest(TOKEN, HOST, "/2.0/events?stream_position=${position}", "GET")
            println("Event Received: ${eventDetails}")
        }
    }

    /*
    This is a generic method for building a HTTP request.
    Param 1: Box Developer Token
    Param 2: Host name
    Param 3: Context Path
    Param 4: HTTP Method Type (Eg: GET, PUT, POST, DELETE, OPTIONS et.)

    This method returns the RAW body of the HTTP Response.
     */

    def static httpRequest(String token, String host, String path, String methodType) {
        HttpRequest httpRequest = new HttpRequest()
        httpRequest
                .method(methodType)
                .protocol("https")
                .host(host)
                .path(path)
                .header(['Authorization': "Bearer ${token}".toString()
        ])
        HttpResponse httpResponse = httpRequest.send()
        httpResponse.body()
    }

    /*
    This is a utility method that takes in a JSON string and the Target Type that it needs to be converted to. We use the Google Gson library for the conversion.
     */

    def static convertStrToJson(String str, type) {
        println("Converting string to JSON")
        return new Gson().fromJson(str, type)
    }
}

/*
This is a representation for the response we receive for the Current Stream position
 */

class Position {
    // Class that holds the elements of a position
    String chunk_size
    String next_stream_position
    List entries
}
